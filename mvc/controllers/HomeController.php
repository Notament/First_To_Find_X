<?php
namespace Controllers;
        class HomeController{
    private $model;

    public function __construct(){
        require_once 'mvc/models/HomeModel.php';
        $this->model = new HomeModel();
    }

    //RAJOUT D'UN STOP EN DB
    public function AddStop(){
        session_start();
        $id_record = $_SESSION['id_record'];
        $lat =  $_POST['lat'];
        $lng =  $_POST['lng'];
        $key =  $_POST['key'];
        $this->model->add_spot($lng,$lat,$id_record,$key);
    }

    //UPDATE D'UN STOP EN DB
    public function UpdateStop(){
        $id_stop = $_POST['id_stop'];
        $stop_name = $_POST['stop_name'];
        $this->model->update_stop($id_stop,$stop_name);
    }

    //RAJOUT D'UN RECORD EN DB
    public function AddRecord(){
        session_start();
        $id_user = $_SESSION['id_user'];
        $add_record = $_POST['add_record'];
        $this->model->add_record($add_record,$id_user);
    }

    //UPDATE D'UN RECORD EN DB
    public function UpdateRecord(){
        $id_record = $_POST['id_record'];
        $record_name = $_POST['record_name'];
        $this->model->update_record($id_record,$record_name);
    }

    //DELETE D'UN RECORD EN DB
    public function DeleteRecord(){
        $id_record = $_POST['id_record'];
        $this->model->delete_record($id_record);
    }

    //DELETE D'UN RECORD EN DB
    public function DeleteStop(){
        $id_stop = $_POST['id_stop'];
        $stop = $this->model->getSpecificSpot($id_stop);
        $this->model->delete_stop($id_stop);
        echo json_encode($stop);
    }

    //UPDATE LA VIEW DU RECORD EN AJAX
    public function UpdateViewRecord(){
        session_start();
        $id_user = $_SESSION['id_user'];
        $last_record = $this->model->updateviewrecord($id_user);
        echo json_encode($last_record);
    }

    //UPDATE LA VIEW DES SPOTS EN AJAX
    public function UpdateViewStop(){
        session_start();
        $id_record = $_SESSION['id_record'];
        $last_stop = $this->model->updateviewstop($id_record);
        echo json_encode($last_stop);
    }

    //GET DE TOUS LES SPOTS ET MISES EN PLACE DE L'ID
    public function GetSpot(){
        session_start();
        $_SESSION['id_record'] = $_POST['id_record'];
        $id_record = $_POST['id_record'];
        $spots = $this->model->getSpot($id_record);
        echo json_encode($spots);
    }

    //DEBUT DU LANCEMENT DE LA PARTIE
    public function Launching(){
        session_start();
        $_SESSION['id_record'] = $_POST['id_record'];
        header('Location: ./Play');
    }

    //LOGOUT
    public function logout(){
        session_start();
        session_destroy();
        header('Location: ./');
    }

    public function insert(){
            session_start();
            //ON RECUPERE LE ID USER POUR SAVOIR QUELS SONT CES CHOSES DEJA DISPONIBLE
            $id_user = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '';
            $info = $this->model->getUserData($id_user);
            //TOUTES LES REQUETES SQL POUR RECUPERER LES DONNEES
            $records = $this->model->getRecord($id_user);
            // On dit à TWIG où se trouvent nos templates
            $loader = new \Twig_Loader_Filesystem('mvc/views');

            $twig = new \Twig_Environment($loader);

            $data = [
                'id_user' => $id_user,
                'records' => $records,
                'info'=>$info,
            ];
            // rendu du fichier views/index.html
            echo $twig->render('HomeView.twig',$data);

            }
        }


?>