<?php
namespace Controllers;
session_start();
class User_SettingController {
    private $model;

//Lien avec le model
    public function __construct(){
        require_once 'mvc/models/User_SettingModel.php';
        $this->model = new User_SettingModel();
    }

    public function updateUserData(){

        //RECUPERATION EN POST DES ELEMENTS
        $login = $_POST['login'];
        $email = $_POST['email'];
        $town = $_POST['town'];
        $password = $_POST['new_pwd'];
        $verifypassword = $_POST['confirm_new_pwd'];
        $id_user = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '';
        $avatar= $_FILES['image'];



        //CHECK DES POTENTIELS ERREURS
        $check = true;

        //ON PASSE TOUT EN HTML SPECIAL CHARS
        $login = htmlspecialchars($login);
        $password = htmlspecialchars($password);
        $verifypassword = htmlspecialchars($verifypassword);
        $email = htmlspecialchars($email);
        $town = htmlspecialchars($town);

        //CHECK QUE LES DEUX PASSWORDS CONCORDENT
        if($password != $verifypassword){
            $_SESSION['false_mdp'] = 'Vos mots de passe ne concordent pas';
            $check = false;
        }


        //CHECK QUE L'EMAIL EST VALIDE
        $regex = "/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/";
        if(preg_match($regex,$email)){
            //NOTHING HAPPEND
        }
        else{
            $_SESSION['false_email'] = 'Votre email est invalide';
            $check = false;
        }

        // ------------------------------- DEBUT UPLOAD ------------------- //
        $hasUpload = $_FILES['image']["size"] > 0;
        if($hasUpload) {
            //CHECK de l'upload du fichier
            $extensionsValides = ['jpg','jpeg','gif','png'];
            $pathinfo = pathinfo($_FILES['image']['name']);
            $extension = strtolower($pathinfo['extension']);

            if (!in_array($extension,$extensionsValides)){
                $_SESSION['false_ext'] = 'Extension de fichier non valide (png ou jpg uniquement)';
                $check = false;
            }

            // 2 - check que le fichier ne fait pas plus de 10MO
            if ($avatar['size'] > 100000) {
                $_SESSION['file_tobig'] = 'Taille du fichier limitée à 100ko';
                $check = false;
                    }

            // 3 - remplacement des caractères spéciaux par des tirets
            $filename = preg_replace('/([^.a-z0-9]+)/i', '-', $avatar['name']);

            // 4 - deplacement du fichier vers uploads
            $destination = "uploads/$filename";
            $resultat = move_uploaded_file($avatar['tmp_name'], $destination);
        }

        // ------------------------------- FIN UPLOAD ------------------- //

            //VERIFICATION De la mise à jour du compte
            if($check){
                $this->model->updateUser($login,$email,$town,$password,$id_user,$destination);
                $_SESSION['register'] = 'Compte mis à jour';
                header('Location: ./Home');
            }
    }

    public function insert(){
        //Recupération des données user
        $id_user = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '';//id_user pour récupérer l'id
        $info = $this->model->getUserData($id_user);
        $mdp_error = isset($_SESSION['false_mdp']) ? $_SESSION['false_mdp'] : '';
        $mail_error = isset($_SESSION['false_email']) ? $_SESSION['false_email'] : '';
        $file_error = isset($_SESSION['false_ext']) ? $_SESSION['false_ext'] : '';
        $file_size = isset($_SESSION['file_tobig']) ? $_SESSION['file_tobig'] : '';
        $register = isset($_SESSION['register']) ? $_SESSION['register'] : '';

        $data= [
        'id_user'=>$id_user,
        'info'=>$info,
        'mdp_error'=>$mdp_error,
        'mail_error'=>$mail_error,
        'file_error'=>$file_error,
        'file_size' => $file_size,
        'register' => $register,
        ];


        //Twig - Lien avec la View
        $loader = new \Twig_Loader_Filesystem('mvc/views');
        $twig = new \Twig_Environment($loader);
        echo $twig->render('User_SettingView.twig',$data);

         //vider les erreurs/succes de la session
         unset($_SESSION['false_mdp']);
         unset($_SESSION['false_email']);
         unset($_SESSION['false_ext']);
         unset($_SESSION['file_tobig']);
         unset($_SESSION['register']);

    }

}

?>