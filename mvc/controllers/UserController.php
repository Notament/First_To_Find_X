<?php
namespace Controllers;
session_start();
class UserController{
    private $model;

    public function __construct($page_active = 'non_form'){
        require_once 'mvc/models/UserModel.php';
        $this->model = new UserModel();
    }

// Register a new user
    public function register(){

        //RECUPERATION EN POST DES ELEMENTS
        $login = $_POST['login'];
        $email = $_POST['email'];
        $town = $_POST['town'];
        $password = $_POST['password'];
        $verifypassword = $_POST['confirm_password'];

        //CHECK DES POTENTIELS ERREURS
        $check = true;

        //ON PASSE TOUT EN HTML SPECIAL CHARS
        $login = htmlspecialchars($login);
        $password = htmlspecialchars($password);
        $verifypassword = htmlspecialchars($verifypassword);
        $email = htmlspecialchars($email);
        $town = htmlspecialchars($town);

        //CHECK QUE LES DEUX PASSWORDS CONCORDENT
        if($password != $verifypassword){
            $_SESSION['false_mdp'] = 'Vos mots de passe ne concordent pas';
            $check = false;
        }

        //CHECK QUE L'EMAIL EST VALIDE
        $regex = "/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/";
        if(preg_match($regex,$email)){
            //NOTHING HAPPEND
        }
        else{
            $_SESSION['false_email'] = 'Votre email est invalide';
            $check = false;
        }

        //VERIFICATION DE LA VALIDATION DE L'INSCRIPTION OU NON
        if($check){
            $a = $this->model->addUser($login,$email,$password,$town);
            var_dump($a);
            $_SESSION['register'] = 'Inscription complétée';
        }
        header('Location: ./#connect');
    }

    public function login(){

        //RECUPERATION EN POST DES ELEMENTS
        $login = $_POST['login'];
        $password = $_POST['password'];

        //ON PASSE TOUT EN HTML SPECIAL CHARS
        $login = htmlspecialchars($login);
        $password = htmlspecialchars($password);

        //VERIFICATION DE LA VALIDATION DE LA CONNEXION
        $tab = $this->model->connectUser($login,$password);

        if(empty($tab)){
            $_SESSION['connexion_fail'] = 'Votre mot de passe ou votre identifiant est incorrecte';
            header('Location: ./#connect');
        }
        else{
            $_SESSION['id_user'] = $tab[0]['id'];
            header('Location: ./Home');
        }

    }
    public function insert(){


        //INITALIATISATION DES ERREURS ET VALIDATION SUR L'INTERFACE DE CONNEXION
        $connect_failure = isset($_SESSION['connexion_fail']) ? $_SESSION['connexion_fail'] : '';
        $mdp_error = isset($_SESSION['false_mdp']) ? $_SESSION['false_mdp'] : '';
        $mail_error = isset($_SESSION['false_email']) ? $_SESSION['false_email'] : '';
        $validate_inscription = isset($_SESSION['register']) ? $_SESSION['register'] : '';

        $data=[
            'connect_failure'=> $connect_failure,
            'mdp_error'=>$mdp_error,
            'mail_error'=>$mail_error,
            'validate_inscription'=>$validate_inscription,
        ];

        // On dit à TWIG où se trouvent nos templates
        $loader = new \Twig_Loader_Filesystem('mvc/views');

        $twig = new \Twig_Environment($loader);

        // rendu du fichier views/index.html
        echo $twig->render('UserView.twig', $data);

        //SESSION DESTROY EN PREVENTION
        session_destroy();
    }
}


?>