<?php
namespace Controllers;

        class PlayController{
    private $model;

    public function __construct(){
        require_once 'mvc/models/PlayModel.php';
        $this->model = new PlayModel();
    }

    public function getStopMap(){
        session_start();
        $spots = $this->model->GetSpots($_SESSION['id_record']);
        echo json_encode($spots);
    }

    public function insert(){
        session_start();
        //ON RECUPERE LE ID USER POUR SAVOIR QUELS SONT CES CHOSES DEJA DISPONIBLE
        $id_user = isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '';
        $spots = $this->model->GetSpots($_SESSION['id_record']);

        // On dit à TWIG où se trouvent nos templates
        $loader = new \Twig_Loader_Filesystem('mvc/views');

        $twig = new \Twig_Environment($loader);

        $data = [
            'spots' => $spots,
            'id_user' => $id_user,
        ];
        // rendu du fichier views/index.html
        echo $twig->render('PlayView.twig',$data);

        }
    }