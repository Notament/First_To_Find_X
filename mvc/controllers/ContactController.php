<?php
namespace Controllers;
session_start();
class ContactController{
    private $model;

    public function __construct(){
        require_once 'mvc/models/ContactModel.php';
        $this->model = new ContactModel();

    }

    public function sendMessage(){

//RECUPERATION EN POST DES ELEMENTS
$email = htmlspecialchars($_POST['email']);
$subject = htmlspecialchars($_POST['subject']);
$message = htmlspecialchars($_POST['message']);

//CHECK DES POTENTIELS ERREURS
$check = true;

//CHECK QUE L'EMAIL EST VALIDE
$regex = "/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/";
if(preg_match($regex,$email)){

$this->model->newMessage($email,$subject,$message);// Envoi le message dans la BD
}
else{
    $_SESSION['false_email'] = 'Votre email est invalide';
    $check = false;
}

if($check){
//Send the Email to an email adresse

    // Create the Transport
    $transport = (new \Swift_SmtpTransport('smtp.mailgun.org', 25))
      ->setUsername('postmaster@sandbox413abab336224b3ebf7c15da48327bb6.mailgun.org')
      ->setPassword('tomate')
    ;

    // Create the Mailer using your created Transport
    $mailer = new \Swift_Mailer($transport);

    // Create a message
    $message = (new \Swift_Message($subject))
      ->setFrom([$email])
      ->setTo(['anneliardet@gmail.com'])
      ->setSubject($subject)
      ->setBody($message)
      ;

    // Send the message
    $result = $mailer->send($message);
    $_SESSION['sucessful_mail'] = 'Mail bien envoyé';
    header('Location: ./Contact#contact');
}
else{
    header('Location: ./Contact#contact');
}
    }
    public function insert(){
        //Erreur de mail
        $mail_error = isset($_SESSION['false_email']) ? $_SESSION['false_email'] : '';
        $sucessful_mail = isset($_SESSION['sucessful_mail']) ? $_SESSION['sucessful_mail'] : '';
        $data=[
            'mail_error'=>$mail_error,
            'successful_mail'=>$sucessful_mail,
        ];

        // On dit à TWIG où se trouvent nos templates
        $loader = new \Twig_Loader_Filesystem('mvc/views');
        
        $twig = new \Twig_Environment($loader);
        
        // rendu du fichier views/index.html
        session_destroy();
        echo $twig->render('ContactView.twig',$data);
            }
}



?>