<?php
namespace Controllers;
class User_SettingModel {
    public function __construct(){
        require_once 'config/config.php';
        //Connection to the database
}

//Retrive  User Data
public function getUserData($id){
        $bdd = connect();
        $req = $bdd->prepare('SELECT * FROM user WHERE id=:id');
        $req->execute(['id' => $id]);
        return $req->fetch();

}

// Update User data's
public function updateUser($login,$email,$town,$password,$id,$avatar){
        if (empty($avatar)){//If image avatar vide  pour empecher les pertes d 'images
            $bdd = connect();
            $req = $bdd->prepare('UPDATE user SET login=:login, email=:email, town=:town, password=:password WHERE id=:id');
            $req->execute(['login'=>$login,'email'=>$email,'town'=>$town,'password'=>$password,'id'=>$id]);

        }
        else if (empty($password)){// if password vide pour empécher les pertes de mot de passe
            $bdd = connect();
            $req = $bdd->prepare('UPDATE user SET login=:login, email=:email, town=:town, avatar=:avatar WHERE id=:id');
            $req->execute(['login'=>$login,'email'=>$email,'town'=>$town,'id'=>$id,'avatar'=>$avatar]);

        }

        else {
                $bdd = connect();
                $req = $bdd->prepare('UPDATE user SET login=:login, email=:email, town=:town, password=:password , avatar=:avatar WHERE id=:id');
                $req->execute(['login'=>$login,'email'=>$email,'town'=>$town,'password'=>$password,'id'=>$id,'avatar'=>$avatar]);
        
            }
        }
    }


?>