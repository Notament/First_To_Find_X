<?php
namespace Controllers;

class UserModel {
    public function __construct($page_active = 'non_form'){
        if($page_active == 'non_form'){require_once 'config/config.php';}
        else{require_once 'config/config.php';}
        //Connection to the database
}

//Add a  user
public function addUser($login,$email,$password,$town){
        $bdd = connect();
        $req = $bdd->prepare('INSERT INTO user VALUES (NULL,:login,:email,:password,:town,NULL)');
        $req->execute(['login' => $login,'email' => $email,'password'=> $password,'town'=>$town]);
        return $req;
    }

//Connect a user
public function connectUser($login,$password){
        $bdd = connect();
        $req = $bdd->prepare('SELECT id  FROM user WHERE login=:log AND password=:pass');
        $req->execute(['log' => $login,'pass' => $password]);
        return $req->fetchAll();
    }


}

?>