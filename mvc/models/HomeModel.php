<?php
namespace Controllers;
include('config/config.php');//Connection to the database
class HomeModel{

    //RAJOUT D'UN STOP EN DB
    public function add_spot($longitude,$latitude,$id_record,$key){
        $bdd = connect();
        $req = $bdd->prepare('INSERT INTO stop VALUES (NULL,:id_record, :point ,:longitude,:latitude,0)');
        $req->execute(['longitude' => $longitude,'latitude' => $latitude,'id_record'=>$id_record,'point'=>'point '.$key]);
    }

    //RAJOUT D'UN RECORD EN DB
    public function add_record($add_record,$id_user){
        $bdd = connect();
        $req = $bdd->prepare('INSERT INTO record VALUES (NULL,:id_user,:add)');
        $req->execute(['id_user' => $id_user,'add' => $add_record]);
    }

    //UPDATE D'UN RECORD EN DB
    public function update_record($id_record,$record_name){
        $bdd = connect();
        $req = $bdd->prepare('UPDATE record SET record_name = :record WHERE id = :id');
        $req->execute(['record' => $record_name,'id' => $id_record]);
    }

    //UPDATE D'UN STOP EN DB
    public function update_stop($id_stop,$stop_name){
        $bdd = connect();
        $req = $bdd->prepare('UPDATE stop SET stop_name = :record WHERE id = :id');
        $req->execute(['record' => $stop_name,'id' => $id_stop]);
    }

    //DELETE D'UN RECORD EN DB
    public function delete_record($id_record){
        $bdd = connect();
        $req = $bdd->prepare('DELETE FROM record WHERE id = :id');
        $req->execute(['id' => $id_record]);
    }

    //DELETE D'UN STOP EN DB
    public function delete_stop($id_stop){
        $bdd = connect();
        $req = $bdd->prepare('DELETE FROM stop WHERE id = :id');
        $req->execute(['id' => $id_stop]);
    }

    //UPDATE LA VIEW DU RECORD EN AJAX
    public function updateviewrecord($id_user){
        $bdd = connect();
        $req = $bdd->prepare('SELECT id,record_name FROM record WHERE id_user = :id_user ORDER BY id DESC LIMIT 1');
        $req->execute(['id_user'=>$id_user]);
        return $req->fetchAll();
    }

    //UPDATE LA VIEW DU RECORD EN AJAX
    public function updateviewstop($id_record){
        $bdd = connect();
        $req = $bdd->prepare('SELECT * FROM stop WHERE id_record = :id_record ORDER BY id DESC LIMIT 1');
        $req->execute(['id_record'=>$id_record]);
        return $req->fetchAll();
    }

    //RECUPERATION DE TOUS LES RECORDS ENREGISTRES EN DB
    public function getRecord($id_user){
        $bdd = connect();
        $req = $bdd->prepare('SELECT id,record_name FROM  record WHERE id_user = :id_user');
        $req->execute(['id_user'=>$id_user]);
        return $req->fetchAll();
    }

    //RECUPERATION DES SPOTS EN FONCTION DE L'ID
    public function getSpot($id_record){
        $bdd = connect();
        $req = $bdd->prepare('SELECT * FROM stop WHERE id_record = :id');
        $req->execute(['id'=>$id_record]);
        return $req->fetchAll();
    }

    //RECUPERATION DU SPOT A DELETE
    public function getSpecificSpot($id_stop){
        $bdd = connect();
        $req = $bdd->prepare('SELECT longitude,latitude FROM stop WHERE id = :id');
        $req->execute(['id'=>$id_stop]);
        return $req->fetchAll();
    }

    //Recuperation des info user
    public function getUserData($id){
        $bdd = connect();
        $req = $bdd->prepare('SELECT * FROM user WHERE id=:id');
        $req->execute(['id' => $id]);
        return $req->fetch();

}
}

?>
