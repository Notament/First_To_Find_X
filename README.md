<--First_To_Find_X-->

<--1/Installation -->
Open a terminal in your directory
	-write $> npm init
	-then $> npm install --only=dev

Create a database FTFX with collation utf8_general.ci
In your php my admin import the FTFX.sql file.
	-Create config.php
	-copy the content of config_example.php
	-modify with your logs
	-save
	-gg

Activer les fichiers .htaccess

éditez le fichier en sudo : /etc/apache2/sites-enabled/000-default.conf

Et ajouter à la fin :

<Directory /var/www/>
 AllowOverride all
</Directory>

Pour que le routeur fonctionne et swiftMailer ainsi que twig fonctionne faire : $> composer install; 
Activez htacess:
-$> ls -l /usr/lib/apache2/modules/
-$> sudo a2enmod rewrite
-$> sudo /etc/init.d/apache2 restart
Puis faire
-faire un dump-autoload

<--2/Basic Design Elements -->

- Font : 
	-Big font:Ubuntu: https://fonts.google.com/specimen/Ubuntu
	-Default font:Raleway: https://fonts.google.com/specimen/Raleway

_ Color:
	- red : rgb(234, 79, 81);
	- grey : rgb(117, 117, 117);
	- black : rgb(42, 35, 35);
