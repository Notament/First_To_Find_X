//ADD RECORD

//QUERY SELECTOR SUR LE FORM ADD RECORD
var formA = document.querySelector(".add_record");
formA.addEventListener("submit", add_record);

//Function ADD en Ajax
function add_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "AddRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {
    //QUAND LE ADD EST EFFECTUE ON UPDATE LA VIEW SANS RECHARGER LA PAGE
    $.ajax({
      method: "POST",
      url: "UpdateViewRecord"
    }).done(function(msg) {
      json = JSON.parse(msg);
      //ON APPEND LE RESULTAT DANS UNE NOUVELLE DIV
      $(".left_frame").append(
        "<div class='records row records row no-gutters' id='" +
          json[0]["id"] +
          "'></div>"
      );
      //POUR CHAQUE RECREATION DE FORM ON LEUR RAJOUTE UN ADD EVENT LISTENER
      $(".records:last").append(
        "<form method='POST' class='update_record event'><div class='record_name'><input type='text' class='form-control' name='record_name' value='" +
          json[0]["record_name"] +
          "'><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-save'></i></button></div></form> "
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", update_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='delete_record event'><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-minus'></i></button></form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", delete_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='add_spot_record event'</form><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-plus'></i></button></form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", add_spot_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='launch_record'</form><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-upload'></i></button></form>"
      );
    });
  });
}

//UPDATE RECORD

//QUERY SELECTOR ALL SUR TOUS LES FORMS UPDATE
var formU = document.querySelectorAll(".update_record");
formU.forEach(function(formulaire) {
  formulaire.addEventListener("submit", update_record);
});

//UPDATE DANS LA BASE DE DONNEE
function update_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "UpdateRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//DELETE RECORD

//QUERY SELECTOR ALL SUR TOUS LES FORMS DELETE

var formD = document.querySelectorAll(".delete_record");
formD.forEach(function(formulaire) {
  formulaire.addEventListener("submit", delete_record);
});

//DELETE DES DONNEES DANS LA DB ET EN VISUEL
function delete_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  //ON SELECTIONNE L'ID QU'ON DELETE GRACE AU FORM PREALABLEMENT RECUPERE
  id_form = form.get("id_record");
  form_remove = document.getElementById(id_form);
  form_remove.remove();
  $("#map").empty();
  //REMOVE EN DB
  $.ajax({
    method: "POST",
    url: "DeleteRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//OUVERTURE DE LA MAP ET RECUPERATION DES POINTS SI DEJA EXISTANT

//QUERY SELECTOR ALL SUR TOUS LES FORMS ADD SPOT

var formD = document.querySelectorAll(".add_spot_record");
formD.forEach(function(formulaire) {
  formulaire.addEventListener("submit", add_spot_record);
});

function add_spot_record(event) {
  event.preventDefault();

  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "GetSpot",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {
    $(".frame").empty();
    init_map();
    json = JSON.parse(msg);
    json.forEach(function(element) {
      $(".frame").append(
        "<div class='stops' id='" + element["id"] + "'></div>"
      );
      $(".stops:last").append(
        "<form method='POST' class='update_stop event'><div class='record_name'><input type='text' class='form-control' name='record_name' value='" +
          element["stop_name"] +
          "'><input type='hidden' name='id_stop' value='" +
          element["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-save'></i></button></div></form> "
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", update_stop);
      $("form").removeClass("event");
      $(".stops:last").append(
        "<form method='POST' class='delete_stop event'><input type='hidden' name='id_stop' value='" +
          element["id"] +
          "'><button class='icon btn btn-dark' type='submit'><i class='fas fa-minus'></i></button></div></form>"
      );
      $(".stops:last").append(
        '<div class="coords">' +
          element["latitude"] +
          "<br>" +
          element["longitude"] +
          "</div>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", delete_stop);
      $("form").removeClass("event");
      var pointCoords = {
        lat: parseFloat(element["latitude"]),
        lng: parseFloat(element["longitude"])
      };
      addMarker(pointCoords, map);
    });
  });
}

//UPDATE DANS LA BASE DE DONNEE
function update_stop(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "UpdateStop",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//DELETE DES DONNEES DANS LA DB ET EN VISUEL
function delete_stop(event) {
  event.preventDefault();
  var form = new FormData(this);
  //ON SELECTIONNE L'ID QU'ON DELETE GRACE AU FORM PREALABLEMENT RECUPERE
  id_form = form.get("id_stop");
  form_remove = document.getElementById(id_form);
  form_remove.remove();
  //REMOVE EN DB
  $.ajax({
    method: "POST",
    url: "DeleteStop",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//Function ADD en Ajax
function add_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "AddRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {
    //QUAND LE ADD EST EFFECTUE ON UPDATE LA VIEW SANS RECHARGER LA PAGE
    $.ajax({
      method: "POST",
      url: "UpdateViewRecord"
    }).done(function(msg) {
      json = JSON.parse(msg);
      //ON APPEND LE RESULTAT DANS UNE NOUVELLE DIV
      $(".right_frame").append(
        "<div class='records' id='" + json[0]["id"] + "'></div>"
      );
      //POUR CHAQUE RECREATION DE FORM ON LEUR RAJOUTE UN ADD EVENT LISTENER
      $(".records:last").append(
        "<form method='POST' class='update_record event'><input type='text' name='record_name' value='" +
          json[0]["record_name"] +
          "'><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><input type='submit' value='Update'</form> "
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", update_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='delete_record event'><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><input type='submit' value='Delete'</form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", delete_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='add_spot_record event'</form><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><input type='submit' value='AddStop'</form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", add_spot_record);
      $("form").removeClass("event");
      $(".records:last").append(
        "<form method='POST' class='launch_record event'</form><input type='hidden' name='id_record' value='" +
          json[0]["id"] +
          "'><input type='submit' value='Launch Record'</form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", launch_record);
      $("form").removeClass("event");
    });
  });
}
//LAUNCH RECORD

function launch_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "Launch",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {
    window.location.href = "Play";
  });
}

//UPDATE RECORD

//QUERY SELECTOR ALL SUR TOUS LES FORMS UPDATE
var formU = document.querySelectorAll(".update_record");
formU.forEach(function(formulaire) {
  formulaire.addEventListener("submit", update_record);
});

//UPDATE DANS LA BASE DE DONNEE
function update_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "UpdateRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//DELETE RECORD

//QUERY SELECTOR ALL SUR TOUS LES FORMS DELETE

var formD = document.querySelectorAll(".delete_record");
formD.forEach(function(formulaire) {
  formulaire.addEventListener("submit", delete_record);
});

//DELETE DES DONNEES DANS LA DB ET EN VISUEL
function delete_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  //ON SELECTIONNE L'ID QU'ON DELETE GRACE AU FORM PREALABLEMENT RECUPERE
  id_form = form.get("id_record");
  form_remove = document.getElementById(id_form);
  form_remove.remove();
  $("#map").empty();
  //REMOVE EN DB
  $.ajax({
    method: "POST",
    url: "DeleteRecord",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//OUVERTURE DE LA MAP ET RECUPERATION DES POINTS SI DEJA EXISTANT

//QUERY SELECTOR ALL SUR TOUS LES FORMS ADD SPOT

var formD = document.querySelectorAll(".add_spot_record");
formD.forEach(function(formulaire) {
  formulaire.addEventListener("submit", add_spot_record);
});

function add_spot_record(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "GetSpot",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {
    $(".frame").empty();
    init_map();
    json = JSON.parse(msg);
    json.forEach(function(element) {
      $(".frame").append(
        "<div class='stops' id='" + element["id"] + "'></div>"
      );
      $(".stops:last").append(
        "<form method='POST' class='update_stop event'><input type='text' name='stop_name' value='" +
          element["stop_name"] +
          "'><input type='hidden' name='id_stop' value='" +
          element["id"] +
          "'><input type='submit' value='Update'</form> "
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", update_stop);
      $("form").removeClass("event");
      $(".stops:last").append(
        '<div class="coords">' +
          element["latitude"] +
          "<br>" +
          element["longitude"] +
          "</div>"
      );
      $(".stops:last").append(
        "<form method='POST' class='delete_stop event'><input type='hidden' name='id_stop' value='" +
          element["id"] +
          "'><input type='submit' value='Delete'</form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", delete_stop);
      $("form").removeClass("event");
      var pointCoords = {
        lat: parseFloat(element["latitude"]),
        lng: parseFloat(element["longitude"])
      };
      addMarker(pointCoords, map);
    });
  });
}

//UPDATE DANS LA BASE DE DONNEE
function update_stop(event) {
  event.preventDefault();
  var form = new FormData(this);
  $.ajax({
    method: "POST",
    url: "UpdateStop",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}

//DELETE DES DONNEES DANS LA DB ET EN VISUEL
function delete_stop(event) {
  event.preventDefault();
  var form = new FormData(this);
  //ON SELECTIONNE L'ID QU'ON DELETE GRACE AU FORM PREALABLEMENT RECUPERE
  id_form = form.get("id_stop");
  form_remove = document.getElementById(id_form);
  form_remove.remove();
  //REMOVE EN DB
  $.ajax({
    method: "POST",
    url: "DeleteStop",
    data: form,
    processData: false,
    contentType: false
  }).done(function(msg) {});
}
