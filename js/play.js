var map;
var labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var labelIndex = 0;
var markers = [];

function getLocation() {
  navigator.geolocation.getCurrentPosition(initialize);
}
function initialize(postion) {
  var CurrentLocation = {
    lat: postion.coords.latitude,
    lng: postion.coords.longitude
  };
  map = new google.maps.Map(document.getElementById("map"), {
    center: CurrentLocation,
    zoom: 20
  });
  addCurentPosition(CurrentLocation, map);
  ShowSpot();
}

function addMarker(location, map) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var marker = new google.maps.Marker({
    position: location,
    label: labels[labelIndex++ % labels.length],
    map: map
  });
}

function addCurentPosition(location, map) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var marker = new google.maps.Marker({
    position: location,
    icon: "assets/avatar.png",
    map: map
  });
}

function ShowSpot() {
  $.ajax({
    method: "POST",
    url: "StopsMap"
  }).done(function(msg) {
    json = JSON.parse(msg);
    json.forEach(function(element) {
      var spots = {
        lat: parseFloat(element["latitude"]),
        lng: parseFloat(element["longitude"])
      };
      var spots_talbe = [
        parseFloat(element["latitude"]),
        parseFloat(element["longitude"])
      ];
      addMarker(spots, map);
      markers.push(spots_talbe);
    });
    console.log(markers);
  });
}

function measure(lat1, lon1, lat2, lon2) {
  // generally used geo measurement function
  var R = 6378.137; // Radius of earth in KM
  var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
  var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * Math.PI / 180) *
      Math.cos(lat2 * Math.PI / 180) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d * 1000; // meters
}

// A = measure(45.69535235936309,4.87520156385995,45.69567927569132,4.87502051475144);
// alert(A);

function watchCurrentPosition() {
  navigator.geolocation.getCurrentPosition(CheckDistance);
}

function CheckDistance(position) {
  markers.forEach(function(element) {
    a = measure(
      position.coords.latitude,
      position.coords.longitude,
      element[0],
      element[1]
    );
    a = Math.round(a);
    console.log(a);
    if (a <= 10) {
      alert("CA MARCHE");
    }
  });
}

setInterval(watchCurrentPosition, 1000);
