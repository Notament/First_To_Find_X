// var x = document.getElementById("demo");
//  function showPosition(position) {
//      console.log(position.coords);
//  }
// function error(err){
//     console.log(err.code,err.message);
// }
// var pos = getLocation();
// console.log(pos);

// function getLocation() {
//   navigator.geolocation.getCurrentPosition(init_map);
// }
var labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var labelIndex = 0;
var markers = [];
var map;

function init_map() {
  var currentLocation = { lat: 10, lng: 10 };
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 1,
    center: currentLocation
  });
  // This event listener calls addMarker() when the map is clicked.
  google.maps.event.addListener(map, "click", function(event) {
    pushMarker(event.latLng, map);
  });

  // Add a marker at the center of the map.
}

// Adds a marker to the map.
function pushMarker(location, map) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var labelzer = labels[labelIndex++ % labels.length];
  var marker = new google.maps.Marker({
    position: location,
    label: labelzer,
    map: map
  });
  // markers.push(marker);
  // console.log(markers);
  $.ajax({
    method: "POST",
    url: "AddStop",
    data: { lat: location.lat(), lng: location.lng(), key: labelzer }
  }).done(function(msg) {
    $.ajax({
      method: "POST",
      url: "UpdateViewStop"
    }).done(function(msg) {
      json = JSON.parse(msg);
      $(".frame").append(
        "<div class='stops' id='" + json[0]["id"] + "'></div>"
      );
      $(".stops:last").append(
        "<form method='POST' class='update_stop event'><input type='text' name='stop_name' value='" +
          json[0]["stop_name"] +
          "'><input type='hidden' name='id_stop' value='" +
          json[0]["id"] +
          "'><input type='submit' value='Update'</form> "
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", update_stop);
      $("form").removeClass("event");
      $(".stops:last").append(
        '<div class="coords">' +
          json[0]["latitude"] +
          "<br>" +
          json[0]["longitude"] +
          "</div>"
      );
      $(".stops:last").append(
        "<form method='POST' class='delete_stop event'><input type='hidden' name='id_stop' value='" +
          json[0]["id"] +
          "'><input type='submit' value='Delete'</form>"
      );
      var eventadd = document.querySelector(".event");
      eventadd.addEventListener("click", delete_stop);
      $("form").removeClass("event");
    });
  });
}

function addMarker(location, map) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var marker = new google.maps.Marker({
    position: location,
    label: labels[labelIndex++ % labels.length],
    map: map
  });
  markers.push(marker);
}

function addCurrentLocation(location, map) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
}

//google.maps.event.addDomListener(window, 'load', initialize);
