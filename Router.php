<?php
use Pecee\SimpleRouter\SimpleRouter;

$prefix = 'projets/First_To_Find_X';

SimpleRouter::group(['prefix' => $prefix], function () {

    // Views
    SimpleRouter::get('/', 'UserController@insert');
    SimpleRouter::get('/Contact', 'ContactController@insert');
    SimpleRouter::get('/Home', 'HomeController@insert');
    SimpleRouter::get('/Setting', 'User_SettingController@insert');
    SimpleRouter::get('/Play', 'PlayController@insert');

    //Forms
    SimpleRouter::post('/Register', 'UserController@register');
    SimpleRouter::post('/Connection', 'UserController@login');
    SimpleRouter::post('/SendContact', 'ContactController@sendMessage');
    SimpleRouter::post('/UpdateProfile', 'User_SettingController@updateUserData');
    SimpleRouter::post('/Launch', 'HomeController@Launching');

    //Ajax
    SimpleRouter::post('/AddStop', 'HomeController@AddStop');
    SimpleRouter::post('/AddRecord', 'HomeController@AddRecord');
    SimpleRouter::post('/UpdateViewRecord', 'HomeController@UpdateViewRecord');
    SimpleRouter::post('/UpdateRecord', 'HomeController@UpdateRecord');
    SimpleRouter::post('/DeleteRecord', 'HomeController@DeleteRecord');
    SimpleRouter::post('/GetSpot' , 'HomeController@GetSpot');
    SimpleRouter::post('/UpdateStop', 'HomeController@UpdateStop');
    SimpleRouter::post('/DeleteStop', 'HomeController@DeleteStop');
    SimpleRouter::post('/UpdateViewStop', 'HomeController@UpdateViewStop');
    SimpleRouter::post('/StopsMap', 'PlayController@getStopMap');


    //Log
    SimpleRouter::get('/Logout', 'HomeController@logout');
});


?>