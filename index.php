
    <?php
    // require_once 'Router.php';
    // Router::dispatch();
/* Load external routes file */
    require_once 'vendor/autoload.php';
    require_once 'Router.php';


    use Pecee\SimpleRouter\SimpleRouter;


/**
 * The default namespace for route-callbacks, so we don't have to specify it each time.
 * Can be overwritten by using the namespace config option on your routes.
 */

    SimpleRouter::setDefaultNamespace('\Controllers');

// Start the routing
    SimpleRouter::start();

//     ?>